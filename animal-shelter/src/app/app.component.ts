import { Component, OnInit } from '@angular/core';
import { Animals } from './models/animals.model';
import { AnimalsService } from './service/animals.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  AnimalArray: Array<Animals> = [];

  constructor(private AnimalService: AnimalsService, private router: Router) { }

  ngOnInit(): void {

    this.AnimalService.getAnimal().subscribe((data: any) => {
      this.AnimalArray = data; 
    })    
  }
}
