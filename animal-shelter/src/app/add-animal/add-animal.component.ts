import { Component } from '@angular/core';
import { Animals } from '../models/animals.model';
import { AnimalsService } from '../service/animals.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-animal',
  templateUrl: './add-animal.component.html',
  styleUrls: ['./add-animal.component.css']
})
export class AddAnimalComponent {
  newAnimal:Animals = new Animals(0,"","","","",new Date (Date.now()),new Date (Date.now()),"");

  constructor(private AnimalsService: AnimalsService){}

  onSubmit(){
    this.AnimalsService.addAnimal(this.newAnimal).subscribe(data => console.dir(data));
    
  }
}
