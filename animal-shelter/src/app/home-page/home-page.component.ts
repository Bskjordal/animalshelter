import { Component, OnInit } from '@angular/core';
import { Animals } from '../models/animals.model';
import { AnimalsService } from '../service/animals.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit{
  AnimalArray: Array<Animals> = [];

  constructor(private AnimalService: AnimalsService, private router: Router) { }

  ngOnInit(): void {

    this.AnimalService.getAnimal().subscribe((data: any) => {
      this.AnimalArray = data;
    })    
  }
}
