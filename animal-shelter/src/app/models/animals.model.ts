export class Animals {
    constructor(public id: number, public name: string, public species: string,public breed: string,public owner: string,public checkInDate: Date,public checkOutDate: Date,public src: string) {        
    }
}
