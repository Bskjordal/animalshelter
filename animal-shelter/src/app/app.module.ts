import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { AddAnimalComponent } from './add-animal/add-animal.component';
import { UpdateAnimalComponent } from './update-animal/update-animal.component';
import { DeleteAnimalComponent } from './delete-animal/delete-animal.component';
import { HttpClientModule } from '@angular/common/http';
import { AnimalsService } from './service/animals.service';

const appRoutes: Routes = [
  { path: "", component: HomePageComponent },
  { path: "animal-add", component: AddAnimalComponent },
  { path: "animal-change", component: UpdateAnimalComponent }, 
  { path: "animal-delete", component: DeleteAnimalComponent }    
];
@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    AddAnimalComponent,
    UpdateAnimalComponent,
    DeleteAnimalComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule
  ],
  providers: [AnimalsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
