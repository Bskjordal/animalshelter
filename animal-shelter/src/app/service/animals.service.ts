import { Injectable } from '@angular/core';
import { Animals } from '../models/animals.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of} from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AnimalsService {

  constructor( private http: HttpClient) { }

  private AnimalList : string = 'http://localhost:5149/api/animal'

  private httpOptions ={
    headers: new HttpHeaders({
      'Contect-Type': 'application/json'
    })
  };

  getAnimal() : Observable<Animals[]> {
    return this.http.get(this.AnimalList,
    this.httpOptions)
    .pipe(map(res => <Animals[]>res));
    }

    addAnimal(newAnimal: Animals) : Observable<Animals[]> {
      return this.http.post(this.AnimalList,
      {
     // id: newAnimal.id,
      name: newAnimal.name,
      species: newAnimal.species,
      breed: newAnimal.breed,
      owner: newAnimal.owner,
      checkInDate: newAnimal.checkInDate,
      checkOutDate: newAnimal.checkOutDate,
      src: newAnimal.src
       },     
      this.httpOptions)
      .pipe(map(res => <Animals[]>res));
    }
}
